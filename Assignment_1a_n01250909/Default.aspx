﻿<%@ Page Language="C#" Inherits="Assignment_1a_n01250909.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Room Reservation</title>
</head>
<body>
    <form id="form" runat="server">
        <div>
            <p>HOTEL ROOM RESERVATION</p>
                <asp:DropDownList runat="server" ID="initials">
                <asp:ListItem Value="Mr." Text="Mr."></asp:ListItem>
                <asp:ListItem Value="Ms." Text="Ms."></asp:ListItem>
                <asp:ListItem Value="Mrs." Text="Mrs."></asp:ListItem>
            </asp:DropDownList>
            <br /> <br />
                First Name:
            <asp:TextBox runat="server" ID="clientfName"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a First Name" ControlToValidate="clientfName" ID="validatorfName"> </asp:RequiredFieldValidator>
                <br /> <br />
                Last Name:
            <asp:TextBox runat="server" ID="clientlName"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Last Name" ControlToValidate="clientlName" ID="validatorlName"> </asp:RequiredFieldValidator>
            <br /> <br />
                Phone Number:
            <asp:TextBox runat="server" ID="clientPhone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number. Please try again"></asp:CompareValidator>
            <br /> <br />
                Email:
            <asp:TextBox runat="server" ID="clientEmail" placeholder="example@gmail.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br /> <br />
                Check In:
            <asp:TextBox runat="server" ID="checkIn" placeholder="dd/mm/yy"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="checkIn" ErrorMessage="Please enter ckeck in date"></asp:RequiredFieldValidator>
            <br /> <br />
                Check Out:
                <asp:TextBox runat="server" ID="checkOut" placeholder="dd/mm/yy"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="checkOut" ErrorMessage="Please enter check out date"></asp:RequiredFieldValidator>
                <br /> <br />
                Choose a room:
            <asp:RadioButton runat="server" Text="Single" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Double" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Queen" GroupName="via"/>
            <asp:RadioButton runat="server" Text="King" GroupName="via"/>
            <br /> <br />
                Pay by:
            <asp:DropDownList runat="server" ID="payOption">
            <asp:ListItem Value="MasterCard" Text="Master Card"></asp:ListItem>
            <asp:ListItem Value="Visa" Text="Visa"></asp:ListItem>
            <asp:ListItem Value="Pay on arrival" Text="Pay on arrival"></asp:ListItem>
            </asp:DropDownList>
            <br /> <br />
                Confirm me by:
            <asp:CheckBox runat="server" ID="contactphone" Text="Call"/>
            <asp:CheckBox runat="server" ID="contactemail" Text="Email"/>     
            <br /> <br />
            <asp:Button runat="server" ID="myButton" Text="Confirm a reservation"/>
            <br /> <br />
            <div runat="server" ID="res"></div>
            
        </div>
    </form>
</body>
</html>
